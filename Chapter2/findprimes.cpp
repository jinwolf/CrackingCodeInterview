#include <iostream>
#include <cmath>

using namespace std;

bool checkPrimeNaive(int n);
bool checkPrimeNaiveBetter(int n);
void printPrimes(int n);

int main() {
	
	cout << checkPrimeNaiveBetter(13);
	cout << "test\n";

	printPrimes(100);
	return 0;
}

bool checkPrimeNaive(int n) {

	if (n < 2) {
		return false;
	}

	for (int i = 2; i < n; i++) {
		if (n % i == 0) {
			return false;
		}
	}

	return true;
}

bool checkPrimeNaiveBetter(int n) {
	if (n < 2) {
		return false;
	}

	for (int i = 2; i < sqrt(n); i++) {
		if (n % i == 0) {
			return false;
		}
	}

	return true;
}

// cross off, what is the idea
// I need an array, I am going to find all prime numbers from 2 to n
// 

void printPrimes(int n) {
	// create a array first
	bool *primes = new bool[n];

	// initialize the arry with true
	for (int i = 0; i < n; i++) {
		primes[i] = true;
	}

	primes[0] = false;
	primes[1] = false;

	for (int i = 2; i < n; i++) {
		if (primes[i] == true) {
			for (int j = 2; i * j < n; j++) {
				primes[i * j] = false;
			}
		}	
	}

	for (int i = 0; i < n; i++) {
		if (primes[i] == true) {
			cout << i << " ";	
		}				
	}

	cout << "\n";
} 



