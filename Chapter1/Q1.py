# Q1. Find if a string has all unique characters without using additional data structure

# brute force approach
def findUniqueString(string):
	i = 0
	for c1 in string:
		i += 1
		for index in range(i, len(string)):
			if c1 == string[index]:
				return False;

	return True;

# note. if you iterate over the string in a nested loop, you will always get False since there is at least one same charater 
# tests
print findUniqueString("jinwoo")
print findUniqueString("abcdef")
print findUniqueString("aabcef")
print findUniqueString("aabcff")
print findUniqueString("novel")

# use a hash table
def findUniqueString2(string):
	charCount = {}
	for c in string:
		if charCount.has_key(c):
			return False
		else:
			charCount[c] = 1
	return True

# tests
print "Test the hashtable approach"
print findUniqueString2("jinwoo")
print findUniqueString2("abcdef")
print findUniqueString2("aabcef")
print findUniqueString2("aabcff")
print findUniqueString2("novel")