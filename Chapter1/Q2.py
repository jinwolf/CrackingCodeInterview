import math
# Q2. implement a function that reverses a string

# this approach uses Python's built-in string function [begin:end:step]
def reverseString(string):
	return string[::-1]

# tests
print reverseString("jinwoo")

# the following won't work in Python since string object in python is immutable
def reverseString2(string):
	stringLen =  len(string)
	for i in range(int(math.floor(stringLen/2))):
		temp = string[i]
		string[i] = string[stringLen - 1 -i]
		string[stringLen - 1 - i] = temp

	return string

# tests
# print reverseString2("abcdef")