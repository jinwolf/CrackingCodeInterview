#include <stdio.h>
#include <string.h>

// checks if the given string consists of unique characters
bool isUniqueString(char* str) {

	// assume we're only dealing with ascii chars which is 8 bits
	// Therefore, there must be more than one duplicate chars if the string lenght is greater than 256
	if (strlen(str) > 256) {
		return false;
	}

	// create placeholders
	bool chars[256];
	for (int i = 0;i < strlen(str); i++) {
		// Below converts charater to corresponding ascii position
		int ascii = (int)str[i];

		if (chars[ascii]) {
			return false;
		} else {
			chars[ascii] = true;
		}
	}
	
	return true;
}

// we can use bit vector 32 bit integer can be used as bit vector holding 32 places
bool isUniqueString2(char* str) {
	int position;
    int check = 0;

    // let's limit charater range from 'a' to 'z'
    for (int i = 0; i < strlen(str); i++) {
    
    	position = (int)(str[i] - 'a');
    
    	if ((check & (1 << position)) > 0) {
    		return false;
    	} else {
    		check |= 1 << position;
    	}
    }

    return true;
}



// Tests
int main() {
	printf("Is %s contains unique chars?: %d\n", "Jinwoo", isUniqueString((char*)"Jinwoo"));
	printf("Is %s contains unique chars?: %d\n", "abcdef", isUniqueString((char*)"abcdef"));
	printf("Is %s contains unique chars?: %d\n", "abcdeff", isUniqueString((char*)"abcdeff"));
	printf("Is %s contains unique chars?: %d\n", "xyz", isUniqueString((char*)"xyz"));

	printf("Is %s contains unique chars?: %d\n", "Jinwoo", isUniqueString2((char*)"Jinwoo"));
	printf("Is %s contains unique chars?: %d\n", "abcdef", isUniqueString2((char*)"abcdef"));
	printf("Is %s contains unique chars?: %d\n", "abcdeff", isUniqueString2((char*)"abcdeff"));
	printf("Is %s contains unique chars?: %d\n", "xyz", isUniqueString2((char*)"xyz"));
	return 0;
}
