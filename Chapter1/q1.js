var assert = require('assert');
// Implement an algorithm to determin if a string has all unique characters
// What if you cannot use additional data structure?

// 1st approach: Using boolean array
function isUnique(word) {
    var check = new Array(256);

    for (var i=0; i < word.length; i++) {
        var charCode = word.charCodeAt(i);

        if (check[charCode]) {
            return false;
        }

        check[charCode] = true;
    }

    return true;
}

describe('isUnique', function() {
    it('should return true if the string has all unique chars', function() {
        assert.equal(isUnique('123456'), true);
        assert.equal(isUnique('abcdef'), true);
        assert.equal(isUnique('aAbBcCdDeEfF'), true);
    });

    it('should return false if the string has all duplicate chars', function() {
        assert.equal(isUnique('Jinwoo'), false);
        assert.equal(isUnique('abcdefa'), false);
        assert.equal(isUnique('1abcdef1234'), false);
    });
});
