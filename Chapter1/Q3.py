# find if the string1 is a permutatio of string2

# sort both string and check if they're the same strings


# create

def isPermutation(string1, string2):
	if len(string1) != len(string2):
		return False

 	# build a hash table where char is the key and the number of occurences are the value
	charSet = {}
	for c in string1:
		if charSet.has_key(c):
			charSet[c] += 1
		else:
			charSet[c] = 1

	# iterate over string 2
	for c in string2:
		if not charSet.has_key(c): # if string2 contains a chracter not found in string1, it returns False
			return False
		elif charSet[c] > 1:
			charSet[c] -= 1 # the number of occurrences is greater than 1 e.g. 2, decrement the value by 1
		else:
			del charSet[c] # if there is only one occurence, removce the char from the hash table

	# in the end, the hash table must be empty
	if len(charSet) == 0:
		return True
	
	return False


# tests
print isPermutation("abcd", "bbbb")
print isPermutation("abcd", "adcb")
print isPermutation("abcd", "aabb")
print isPermutation("abcd", "dcba")

